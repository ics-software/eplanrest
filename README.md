# README #

This rest interface reads data from a EPlan MSSQL database and produces a XML file containing all
defined parts for cables.
The data is consumed by the Cable DB applications that maps internal cable types to the "commercial" equivalents.

| MSSQLDB |  ==> eplanrest ==> XML ==> Cable App ==> | CableDB |      

This README would normally document whatever steps are necessary to get your application up and running.
Each XML node (within parts>):

<parts>
   <part id="3M.1SF26-L120-00C-100">
     <erpnr/>
     <typenr>1SF26-L120-00C-100</typenr>
     <description1>@High Speed Digital Data Transmission, 26 position;</description1>
     <externaldocument1>http://solutions.3m.co.uk/wps/portal/3M/en_GB/Interconnect-EU/Home/Solutions/MachineVision/</externaldocument1>
     <manufacturer>3M</manufacturer>
     </part>
</parts>

### How do I get set up? ###

This Eplan rest interface uses some system variables that needs to be set by the application server.
System variables may be set in:
* standalone.xml (running wildfly)
* catalina.properties (running tomcat)

eplan.jdbc.driver", "net.sourceforge.jtds.jdbc.Driver");
eplan.jdbc.url=jdbc:jtds:sqlserver://MSSQL01.ESSS.LU.SE;DatabaseName=eplan_parts_v27;domain=ESSS.LU.SE");
eplan.jdbc.user=xxx
eplan.jdbc.password=xxx

### Contribution guidelines ###

Example usage:
http://localhost:8080/eplanrest/rest/parts

Example consuming data:
See eplanrest.html sample file.


Questions: Icsscontrolsystemsupport@esss.se
