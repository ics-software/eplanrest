/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.ics.cceco.eplan.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import se.esss.ics.cceco.eplan.domain.Part;
import se.esss.ics.cceco.eplan.util.DbUtil;

/**
 *
 * @author janakepersson
 */
public class PartDao {
    private Connection connection;
    //private Logger logger;
    public PartDao(Properties properties) {
        super();
        this.connection = new DbUtil().getConnection(properties);
        //this.logger = new Utilities().getLogger(properties);
    }
       public List<Part> getAllParts() {
        List<Part> parts = new ArrayList<Part>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select erpnr,manufacturer,partnr,typenr,description1, externaldocument1 from tblPart where productgroup=29 order by manufacturer");
            while (rs.next()) {
                Part part = new Part();
                
                part.setErpnr(rs.getString("erpnr"));
                part.setManufacturer(rs.getString("manufacturer"));
                part.setPartnr(rs.getString("partnr"));
                part.setTypenr(rs.getString("typenr"));
                part.setDescription1(rs.getString("description1"));
                part.setExternaldocument1(rs.getString("externaldocument1"));

                parts.add(part);
            }
        } catch (NullPointerException | SQLException e) {
            //this.logger.log(Level.SEVERE, e.toString());
            e.printStackTrace();
        }

        return parts;
    }

}
