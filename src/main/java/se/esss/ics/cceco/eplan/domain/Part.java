/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.ics.cceco.eplan.domain;

/**
 *
 * @author janakepersson
 */
public class Part {

    public Part() {
        
    }
    public Part(String partnr, String erpnr, String typenr, String description1, String externaldocument1, String manufacturer) {
        this.setPartnr(partnr);
        this.setErpnr(erpnr);
        this.setTypenr(typenr);
        this.setDescription1(description1);
        this.setExternaldocument1(externaldocument1);
        this.setManufacturer(manufacturer);
    }

    public String getPartnr() {
        return partnr;
    }

    public void setPartnr(String partnr) {
        this.partnr = partnr;
    }

    public String getErpnr() {
        return erpnr;
    }

    public void setErpnr(String erpnr) {
        this.erpnr = erpnr;
    }

    public String getTypenr() {
        return typenr;
    }

    public void setTypenr(String typenr) {
        this.typenr = typenr;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getExternaldocument1() {
        return externaldocument1;
    }

    public void setExternaldocument1(String externaldocument1) {
        this.externaldocument1 = externaldocument1;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    /**
     * 
     * @return an XML header for all part(s)
     * <pre>
     *  &lt;?xml version=\&quot;1.0\&quot; encoding=\&quot;UTF-8\&quot; standalone=\&quot;no\&quot; ?&gt;
     *  &lt;parts&gt;
     * </pre>
     */
    public String getXMLHead() {
         return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>" +
        "<parts>";
    }
    /**
     * 
     * @return an XML footer for all part(s) 
     * <pre>
     * &lt;/parts&gt;
     * </pre>
    */
    public String getXMLFoot() {
         return "</parts>";
    }
    /**
     * 
     * @return One part object formatted as an xml node
     * <pre>
     * &lt;part id=&quot;3M.1SF26-L120-00C-100&quot;&gt;
     * &lt;erpnr/&gt;
     * &lt;typenr>1SF26-L120-00C-100&lt;/typenr&gt;
     * &lt;description1&gt;@High Speed Digital Data Transmission, 26 position;&lt;/description1&gt;
     * &lt;externaldocument1&gt;http://solutions.3m.co.uk/wps/portal/3M/en_GB/Interconnect-EU/Home/Solutions/MachineVision/&lt;/externaldocument1&gt;
     * &lt;manufacturer>3M&lt;/manufacturer&gt;
     * &lt;/part&gt;
     * </pre>
     */
    public String toXML()
    {
         return 
	"<part id=\"" + this.partnr + "\">" +
	"<erpnr>" + this.erpnr + "</erpnr>" +
	"<typenr>" + this.typenr + "</typenr>" +
//	"<description1>" + this.description1 + "</description1>" +
//	"<externaldocument1>" + this.externaldocument1 + "</externaldocument1>" +
	"<manufacturer>" + this.manufacturer + "</manufacturer> " +
	"</part>";
    }

    private String partnr = null; //"LAPP.0038606";
    private String erpnr = null; //"ESS-01010101";
    private String typenr = null; //"UNITRONIC® LiHCH(TP)";
    private String description1 = null; //"6X2X0,5;da_DK@UNITRONIC LiHCH(TP) 6X2X0";
    private String externaldocument1 = null; //"http://www.lappkabel.de/nc/produktsuche/artikelnummernsuche.html?tx_dgpwebcatalog09_pi4[article]=0038606";
    private String manufacturer = null; //"LAPP";
}
