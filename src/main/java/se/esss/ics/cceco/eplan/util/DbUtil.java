/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.ics.cceco.eplan.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author janakepersson
 */
public class DbUtil {
    private Connection connection = null;

    /*
    Note! There are two ways of getting a jdbc connection
    "old" properties is used to load driver
    Class.forName(driver);
    connection = DriverManager.getConnection(url, user, password);
    where properties should be set in e.g. system property section of Wildfly standalone.xml

    "new" configured datasource in java EE, e.g. <datasource> in wildfly, <Resource name="jdbc/TestDB" in Tomcat
    InitialContext context = new InitialContext();
    DataSource datasource = (DataSource) context.lookup("java:/MySqlDS");

    try (final Connection connection = datasource.getConnection();) {
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);   
    }
    */
    /**
     * 
     * @param properties containing 4 parameters:
     * <ol>
     * <li>jdbc.driver</li>
     * <li>jdbc.url</li>
     * <li>jdbc.user</li>
     * <li>jdbc.password</li>
     * </ol>
     * @return a Singleton Connection object
     */
    public Connection getConnection(Properties properties) {
        if (connection != null)
            return connection;
        else {
                String driver = properties.getProperty("jdbc.driver");
                String url = properties.getProperty("jdbc.url");
                String user = properties.getProperty("jdbc.user");
                String password = properties.getProperty("jdbc.password");
            try {
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, " Connected to " + url);

            } catch (ClassNotFoundException | SQLException e) {
                Logger.getLogger(this.getClass().getSimpleName()).log(Level.SEVERE, e.toString());
                e.printStackTrace();
            }
            return connection;
        }

    }
}
