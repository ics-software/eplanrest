/*
 *  Copyright (C) 2018 European Spallation Source ERIC.
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package se.esss.ics.cceco.eplan.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import se.esss.ics.cceco.eplan.domain.Part;
import se.esss.ics.cceco.eplan.dao.PartDao;
import java.util.Properties;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

/**
 * This resource provides bulk Eplan parts data by reading the Eplan MSSQL
 * database.
 * @see se.esss.ics.cceco.eplan.domain.Part
 * @author janakepersson
 */
@Produces("application/xml")
@Path("/parts")
public class EplanService {

    @Context
    UriInfo uri; //e.g. http://localhost:8080/eplanrest/rest/parts

    @GET
    public Response getXMLParts() {
        Properties sysProp = System.getProperties();
        sysProp.list(System.out);
        //NOTE! properties should be configured in wildfly standalone.xml or file setenv.sh in in <tomcat-install-dir>/bin directory and read from sysProp above!
        if (sysProp.getProperty("eplan.jdbc.driver") == null) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "uri: " + uri.getBaseUri() + ", path: " + uri.getPath() + ", env variable eplan.jdbc.driver   missing for database connection to Eplan!");
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "uri: " + uri.getBaseUri() + ", path: " + uri.getPath() + ", env variable eplan.jdbc.url      missing for database connection to Eplan!");
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "uri: " + uri.getBaseUri() + ", path: " + uri.getPath() + ", env variable eplan.jdbc.user     missing for database connection to Eplan!");
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "uri: " + uri.getBaseUri() + ", path: " + uri.getPath() + ", env variable eplan.jdbc.password missing for database connection to Eplan!");
            return null;
        } else {

            Properties prop = new Properties();
            prop.setProperty("jdbc.driver", sysProp.getProperty("eplan.jdbc.driver") );
            prop.setProperty("jdbc.url", sysProp.getProperty("eplan.jdbc.url") );
            prop.setProperty("jdbc.user", sysProp.getProperty("eplan.jdbc.user") );
            prop.setProperty("jdbc.password", sysProp.getProperty("eplan.jdbc.password") );

            PartDao partDao = new PartDao(prop);
            List<Part> parts = partDao.getAllParts();
            StringBuilder sb = new StringBuilder();
            if (parts.size() > 0) {
                sb.append(parts.get(0).getXMLHead());
                for (Part p : parts) {
                    sb.append(p.toXML());
                }
                sb.append(parts.get(0).getXMLFoot());
            }

            ResponseBuilder response = Response.accepted();
            response.header("Access-Control-Allow-Origin", "*"); //CORS, Allow any web-server to access this service   
            //Note! This rest service returns status code 202, i.e. if used from within a javascript this may have to be considered/check
            //else we could change the status code in the header
            response.entity(sb.toString());

            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "uri: " + uri.getBaseUri() + ", path: " + uri.getPath() + ", returned " + parts.size() + " records");
            return response.build();
//        return sb.toString();
        }
    }
}
